#!/usr/bin/perl

## get filename
die unless @ARGV==1;
$file = $ARGV[0];

## 1 atomic unit time = 2.418884326505e-17 s
## prefactor, in microsecond^-1
$prefactor = 4.0/3.0 * 1.0/137.036**3 / 2.418884e-11 ;

## read operator, energy and transition moment
open FL,"$file.out" or die;
$i = 0;
$j = 0;
$k = 0;
while(<FL>){
  if(/A operator label/){ 
    $_ = (split ":",$_)[1];
    $a[$i] = (split)[0];
    $i++;
  }
  if(/B operator label/){
    $_ = (split ":",$_)[1];
    $b[$j] = (split)[0];
    $j++;
  }
  if(/omega B/){
    $_ = (split ":",$_)[1];
    $e[$k] = (split)[1];
    $m[$k] = (split)[2];
    $k++;
  }
}
if($i!=$j or $j!=$k or $k!=$i){ print "i/=j or j/=k or k/=i!"; exit; }
$n = $i-1;
close FL;

## calculate oscillator strength, rate constant and lifetime 
@operator = qw/X1SPNSCA Y1SPNSCA Z1SPNSCA/;
for $j (0..2) {
  $oscf[$j] = 0.0;
  $rate[$j] = 0.0;
  for $i (0..$n) {
    if($b[$i] eq $operator[$j]){ 
      $oscf[$j] += $e[$i] * $m[$i]**2; 
      $rate[$j] += $e[$i]**3 * $m[$i]**2; 
    }
  }
  $oscf[$j] *= 2.0/3.0;
  $rate[$j] *= $prefactor;
  $tau[$j] = 1.0/$rate[$j];
}
$lifetime = 3.0/($rate[0]+$rate[1]+$rate[2]);

## print results
print  "---------------------------------\n";
print  " $file \n";
print  "---------------------------------\n";
printf "%5s%9s%9s%9s\n", "", "X", "Y", "Z";
printf "%5s%9.1e%9.1e%9.1e\n", "oscf", $oscf[0], $oscf[1], $oscf[2];
printf "%5s%9.1e%9.1e%9.1e  %-15s\n", "rate", $rate[0]*10**6, $rate[1]*10**6, $rate[2]*10**6, "s^-1";
printf "%5s%9.1f%9.1f%9.1f  %-15s\n", "tau", $tau[0], $tau[1], $tau[2], "10^-6 s";
print  "---------------------------------\n";
printf " ave_rate = %8.2f  *10^5 s^-1\n", 10.0*($rate[0]+$rate[1]+$rate[2])/3.0;
printf " lifetime = %8.2f microsecond\n", $lifetime;
printf " emission = %8.2f eV %5.0f nm\n", 27.2114*$e[0], 1240.0/27.2114/$e[0];
print  "---------------------------------\n";

## end
