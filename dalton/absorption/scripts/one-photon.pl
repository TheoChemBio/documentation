#!/usr/bin/perl

use strict;
use warnings;
use Math::Trig;

# only 1 excited states
my $nstates = 1;
my (@osc_str, @excite);

# average TPA cross section
my $ave_ene_s1 = 0.0;
my $ave_dip_s1 = 0.0;
my $ave_osc_s1 = 0.0;
my $count_ave = 0;

# Read DALTON output files

$_ = `ls opa*out`;
my @file = split;

my ($ifile, $frame);
for $ifile (0 .. @file-1)
{
    #printf "Parsing output file $file[$ifile] ...\n";

    # get frame 
    if($file[$ifile] =~ /W_(\d+)\.out$/)
    {
        $frame = $1;
    }

    # check normal termination
    $_ = `tail $file[$ifile] | grep wall`;
    unless(/DALTON/){ next; }

    # Extract TPA tensor

    my ($s, $a, $b);

    open FL,"$file[$ifile]" or die;
    while (<FL>)
    {
        if(/Excited state no:\s+(\d+)\s+/)
        {
            $s = $1;
        }

        if(/Excitation energy :(.*?)au/)
        {
            $excite[$s] = $1;
            $osc_str[$s] = 0.0;
        }

        if(/Oscillator strength \(LENGTH\)   :(.*?)\(Transition moment :/)
        {
            $osc_str[$s] += $1;
        }
    }
    close FL;

    for $s (1 .. $nstates)
    {
        my $dip_str = $osc_str[$s] * 1.5 / $excite[$s];
        printf "%5d  %.3f  %.3f  %.3f\n", $frame, $dip_str, $excite[$s]*27.21138505, $osc_str[$s];

        if($s == 1) 
        { 
            $ave_dip_s1 += $dip_str;
            $ave_ene_s1 += $excite[$s]*27.21138505;
            $ave_osc_s1 += $osc_str[$s]; 
            $count_ave ++; 
        }
    }
}

printf "#AVE OPA = %.3f  %.3f  %.3f\n", 
        $ave_dip_s1 / $count_ave,
        $ave_ene_s1 / $count_ave,  
        $ave_osc_s1 / $count_ave;

