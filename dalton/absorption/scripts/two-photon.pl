#!/usr/bin/perl

use strict;
use warnings;
use Math::Trig;

my @ijk2xyz = ('X', 'Y', 'Z');
my %xyz2ijk = ('X', 0, 'Y', 1, 'Z', 2);

# only 1 excited states
my $nstates = 1;
my (@moment, @excite);

# average TPA cross section
my $ave_tpa_s1 = 0.0;
my $count_ave = 0;

# Read DALTON output files

$_ = `ls tpa*out`;
my @file = split;

my ($ifile);
for $ifile (0 .. @file-1)
{
    #printf "Parsing output file $file[$ifile] ...\n";

    # check normal termination
    $_ = `tail $file[$ifile] | grep wall`;
    unless(/DALTON/){ next; }

    # Extract TPA tensor

    open FL,"$file[$ifile]" or die;
    while (<FL>)
    {
        if(/Second order moment in a\.u\. for/)
        {
            my ($s, $a, $b);

            $_ = <FL>;
            die unless (/A operator label/);
            if   (/XDIPLEN/) { $a = 0; }
            elsif(/YDIPLEN/) { $a = 1; }
            elsif(/ZDIPLEN/) { $a = 2; }
            else {die "Error in line: $_";};

            $_ = <FL>;
            die unless (/B operator label/);
            if   (/XDIPLEN/) { $b = 0; }
            elsif(/YDIPLEN/) { $b = 1; }
            elsif(/ZDIPLEN/) { $b = 2; }
            else {die "Error in line: $_";};

            $_ = <FL>;
            die unless (/Excited state no\./);
            if   (/spin:\s+(\d+)\s+/) { $s = $1; }
            else {die "Error in line: $_";};

            $_ = <FL>;

            $_ = <FL>;
            die unless (/omega B/);
            @_ = split;
            $moment[$s][$a][$b] = $_[@_-1];
            $excite[$s] = $_[@_-2];
        }
    }
    close FL;

    # Check beta components ...

    my ($s, $a, $b);
    for $s (1 .. $nstates)
    {
        for $a (0 .. 2)
        {
            for $b (0 .. 2)
            {
                if ((not defined $moment[$s][$a][$b]) or
                     (not defined $excite[$s]))
                {
                    printf "Error in state[%d], comp[%d][%d]\n", 
                            $s, $a, $b;
                    exit;
                }
            }
        }
    }

    # calculate isotropic TPA (linearly polarized)

    my @tpa;
    for $s (1 .. $nstates)
    {
        $tpa[$s] = 0.0;
        for $a (0 .. 2)
        {
            for $b (0 .. 2)
            {
                $tpa[$s] += $moment[$s][$a][$a] *  $moment[$s][$b][$b] * 2.0 +
                            $moment[$s][$a][$b] *  $moment[$s][$a][$b] * 2.0 +
                            $moment[$s][$a][$b] *  $moment[$s][$b][$a] * 2.0;
            }
        }
        $tpa[$s] /= 30.0;
    }

    # Write beta components to dat file

    my $datfile = $file[$ifile];
    $datfile =~ s/\.out$/.dat/;
    open DAT,">$datfile" or die;

    for $s (1 .. $nstates)
    {
        for $a (0 .. 2)
        {
            for $b (0 .. 2)
            {
               printf DAT "%3d   %3d %3d   %22.12e   %22.12e\n", 
                        $s, $a, $b, $moment[$s][$a][$b], $excite[$s];
            }
        }
    }
    #
    # constants from CODATA:
    #   Fine structure constant: alpha = 7.2973525698e-3
    #   Bohr radius: a0 = 5.291772108e-9 cm
    #   Speed of light: c = 2.99792458e+10 cm/s
    #   1 Hartree = 27.21138505 eV
    #
    # assuming Gamma=0.1eV for Lorentzian broadening
    # conv_factor = (2.0*pi)**3 * $alpha * $a0**5 / ($c * pi * 0.1/27.21138505)
    #             = 2.17015458738528
    #
    # TPA_GM = conv_factor * ene**2 * TPA_au
    # NOTE: ene is equal to 0.5*excite due to two-photon
    #
    for $s (1 .. $nstates)
    {
        my $this_tpa = $tpa[$s] * $excite[$s]**2 * 2.17015458738528 / 4.0;
        printf "%s  %.3f  %.1f\n", $file[$ifile], $excite[$s]*27.21138505, $this_tpa;

        printf DAT "STATE %3d  TPA_LINEAR %22.12e\n", $s, $this_tpa;
        if($s == 1) { $ave_tpa_s1 += $this_tpa; $count_ave ++; }
    }
    close DAT;
}

printf "#AVE TPA = %.0f\n", $ave_tpa_s1 / $count_ave;

