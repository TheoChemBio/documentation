#!/usr/bin/perl

   use strict;
   use warnings;
   use Math::Trig;

   my ($file, $frame, $g_shift_found, $i, $j, $k, $aii, $aji);
   my (@g_shift, @pa, @new_g_shift, @rot_25);

#  get filenames
   my @outfiles=`ls *.gtensor.out`;
   my $nFiles=@outfiles;
   my $nFrames=@outfiles;
   for $file ( 1 .. @outfiles )
   {
      chomp($outfiles[$file-1]);
      $outfiles[$file-1] =~ s/W_//;
      $outfiles[$file-1] =~ s/\.qmmm\.gtensor\.out//;
   }

   my @term=qw/RMC GC1 OZ-SO1 OZ-SO2 Total/;
   printf "========================================\n";
#  printf "%-8s%8s%8s%8s%8s\n", " ", "xx", "yy", "zz", "iso";

   for my $it ( 1 .. 1 )
   {

      my @aver_g_shift;
      for $i ( 0 .. 8 )
      {
         $aver_g_shift[$i] = 0.0;
      }
 
      my $g_shift_iso_before = 0.0;
      my $g_shift_iso_after = 0.0;
 
      for $file ( 1 .. $nFiles )
      {
         $frame = $outfiles[$file-1];
 
##       read coordinates and compute N-O vector
         my (@a,@x,@y,@z);
         my $atom = 0;
         my $mol_file="W_".$frame."_mol.inp";
         open MOL,"$mol_file" or die "Cannot open file $mol_file !\n";
         while ( <MOL> )
         {
            if ( /^C / or /^H / or /^O / or /^N / or /^S / )
            {
               $atom++;
               $a[$atom] = (split)[0];
               $x[$atom] = (split)[1];
               $y[$atom] = (split)[2];
               $z[$atom] = (split)[3];
            }
         }
         close MOL;
         die "Incorrect number of atoms!\n" unless $atom==71;
 
         my $r_no=sqrt( ($x[60]-$x[66])**2 
                      + ($y[60]-$y[66])**2 
                      + ($z[60]-$z[66])**2 );
         my @vec_x=( ($x[60]-$x[66])/$r_no,
                     ($y[60]-$y[66])/$r_no, 
                     ($z[60]-$z[66])/$r_no );
 
         my $r_cc=sqrt( ($x[1]-$x[2])**2 
                      + ($y[1]-$y[2])**2 
                      + ($z[1]-$z[2])**2 );
         my @vec_y=( ($x[1]-$x[2])/$r_cc, 
                     ($y[1]-$y[2])/$r_cc,
                     ($z[1]-$z[2])/$r_cc );
 
         my @vec_z=out_p(@vec_x,@vec_y);
         my $r_vec_z = sqrt($vec_z[0]**2+$vec_z[1]**2+$vec_z[2]**2);
         $vec_z[0] /= $r_vec_z;
         $vec_z[1] /= $r_vec_z; 
         $vec_z[2] /= $r_vec_z;

         @vec_y=out_p(@vec_z,@vec_x);
 
##       read g-shift (principal axes and values) from output
         $g_shift_found = 0;
         open FL,"W_$frame.qmmm.gtensor.out"
            or die "Cannot open file W_$frame.qmmm.gtensor.out !\n";
         while ( <FL> )
         {
 
#           @G           xx     yy     zz     xy     yx     xz     zx     yz     zy
#           @G RMC     -274.  -274.  -274.     0.     0.     0.     0.     0.     0.
#           @G GC1      135.  2307.  3105.  -529. -2018.   878. -1352.   705.  -279.
#           @G OZ-SO1  6957.  4971.  2135.  1770.   297. -4329. -6331. -1347. -2579.
#           @G OZ-SO2 -3633. -3132.   610.    55.   516.  1015.  3590.   307.  1514.
#           @G Total   3185.  3871.  5576.  1296. -1205. -2436. -4093.  -335. -1344.
 
            if ( /^\@G $term[$it]/ )
            {
               @_ = (split);
          
               $g_shift[0] = $_[2];
               $g_shift[4] = $_[3];
               $g_shift[8] = $_[4];
          
               $g_shift[1] = ($_[5]+$_[6])/2.0;
               $g_shift[3] = ($_[5]+$_[6])/2.0;
          
               $g_shift[2] = ($_[7]+$_[8])/2.0;
               $g_shift[6] = ($_[7]+$_[8])/2.0;
          
               $g_shift[5] = ($_[9]+$_[10])/2.0;
               $g_shift[7] = ($_[9]+$_[10])/2.0;

               $g_shift_found = 1;
               last;
            }

         }
         close FL;
         die "g-shift-tensor not found in frame $frame !\n" 
            if ( $g_shift_found == 0 );

         $g_shift_iso_before += ($g_shift[0]+$g_shift[4]+$g_shift[8])/3;

##       define rotation matrix according to N-O and C-C axes
         @pa = ((@vec_x), (@vec_y), (@vec_z));
 
##       rotate g-shift tensor according to N-O and C-C axes
         (@new_g_shift) = mmult(mmult(@pa,@g_shift),mT(@pa));

         printf "%d%8.1f%8.1f%8.1f%8.1f\n", $frame,
                $new_g_shift[0],$new_g_shift[4],$new_g_shift[8],
                ($new_g_shift[0]+$new_g_shift[4]+$new_g_shift[8])/3.0;
 
         $g_shift_iso_after += ($new_g_shift[0]+$new_g_shift[4]+$new_g_shift[8])/3;

         for $i ( 0 .. 8 )
         {
            $aver_g_shift[$i] += $new_g_shift[$i];
         }
 
      }
 
      for $i ( 0 .. 8 )
      {
         $aver_g_shift[$i] /= $nFrames;
      }

   }
   printf "========================================\n";


#  matrix multiplication
   sub mmult
   {
      die "Incorrect number of elements in mmult!\n"
         if @_!=18;
      my ($i,$j,$k);
      my (@a,@b,@c);
      for $i ( 0 .. 8 )
      {
         $a[$i] = $_[$i];
         $b[$i] = $_[$i+9];
      }
      for $i ( 0 .. 2 )
      {
         for $j ( 0 .. 2 )
         {
            $c[$i*3+$j] = 0.0;
            for $k ( 0 .. 2 )
            {
               $c[$i*3+$j] += $a[$i*3+$k] * $b[$k*3+$j];
            }
         }
      }
      return (@c);
   }

#  matrix transpose
   sub mT
   {
      my ($i,$j);
      my @a;
      for $i ( 0 .. 2 )
      {
         for $j ( 0 .. 2 )
         {
            $a[$j*3+$i] = $_[$i*3+$j];
         }
      }
      return (@a);
   }

#  print matrix
   sub mprint
   {
      for $i ( 0 .. 2 )
      {
         for $j ( 0 .. 2 )
         {
            printf "%10.2f", $_[$i*3+$j];
         }
         printf "\n";
      }
   }

#  outer product
   sub out_p
   {
      my ($i);
      my (@a,@b,@c);
      for $i ( 0 .. 2 )
      {
         $a[$i] = $_[$i];
         $b[$i] = $_[$i+3];
      }
      $c[0] = $a[1]*$b[2] - $a[2]*$b[1];
      $c[1] = $a[2]*$b[0] - $a[0]*$b[2];
      $c[2] = $a[0]*$b[1] - $a[1]*$b[0];
      return (@c);
   }
