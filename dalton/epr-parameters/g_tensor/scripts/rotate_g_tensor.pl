#!/usr/bin/perl

   use strict;
   use warnings;
   use Math::Trig;

   my ($file, $frame, $g_shift_found, $i, $j, $k, $aii, $aji);
   my (@g_shift, @pa, @new_g_shift);

#  get filenames
   my @outfiles=`ls *.gtensor.out`;
   my $nFiles=@outfiles;
   my $nFrames=@outfiles;
   for $file ( 1 .. @outfiles )
   {
      chomp($outfiles[$file-1]);
      $outfiles[$file-1] =~ s/W_//;
      $outfiles[$file-1] =~ s/\.qmmm\.gtensor\.out//;
   }

   my @aver_g_shift;
   for $i ( 0 .. 8 )
   {
      $aver_g_shift[$i] = 0.0;
   }

   my $g_shift_iso_before = 0.0;
   my $g_shift_iso_after = 0.0;

   for $file ( 1 .. $nFiles )
   {
      $frame = $outfiles[$file-1];
      printf "\nFrame $frame\n";

#     read coordinates and compute N-O vector
      my (@a,@x,@y,@z);
      my $atom = 0;
      my $mol_file="W_".$frame."_mol.inp";
      open MOL,"$mol_file" or die "Cannot open file $mol_file !\n";
      while ( <MOL> )
      {
         if ( /^C / or /^H / or /^O / or /^N / or /^S / )
         {
            $atom++;
            $a[$atom] = (split)[0];
            $x[$atom] = (split)[1];
            $y[$atom] = (split)[2];
            $z[$atom] = (split)[3];
         }
      }
      close MOL;
      die "Incorrect number of atoms!\n" unless $atom==71;

      my $r_no=sqrt( ($x[60]-$x[66])**2 
                   + ($y[60]-$y[66])**2 
                   + ($z[60]-$z[66])**2 );
      my @vec_x=( ($x[60]-$x[66])/$r_no,
                  ($y[60]-$y[66])/$r_no, 
                  ($z[60]-$z[66])/$r_no );

      my $r_cc=sqrt( ($x[1]-$x[2])**2 
                   + ($y[1]-$y[2])**2 
                   + ($z[1]-$z[2])**2 );
      my @vec_y=( ($x[1]-$x[2])/$r_cc, 
                  ($y[1]-$y[2])/$r_cc,
                  ($z[1]-$z[2])/$r_cc );

#     NOTE: x and y are not necessarily perpendicular
#           so z must be normalized!
      my @vec_z=out_p(@vec_x,@vec_y);
      my $r_vec_z = sqrt($vec_z[0]**2+$vec_z[1]**2+$vec_z[2]**2);
      $vec_z[0] /= $r_vec_z;
      $vec_z[1] /= $r_vec_z;
      $vec_z[2] /= $r_vec_z;

      @vec_y=out_p(@vec_z,@vec_x);

#     read g-shift (principal axes and values) from output
      $g_shift_found = 0;
      open FL,"W_$frame.qmmm.gtensor.out"
         or die "Cannot open file W_$frame.qmmm.gtensor.out !\n";
      while ( <FL> )
      {
         if ( /direction cosines/ )
         {
            print;
            for $i ( 0 .. 2 )
            {
               $_ = <FL>; print;
               for $j ( 0 .. 2 )
               {
                  @_=(split);
                  $pa[$i*3+$j] = $_[$j+3];
                  $g_shift[$i*3+$j] = $_[1]*10**6 if $i==$j;
                  $g_shift[$i*3+$j] = 0.0 if $i!=$j;
               }
            }
            $g_shift_found = 1;
#           rotate g-shift tensor back to original axes
            (@g_shift) = mmult(mmult(mT(@pa),@g_shift),@pa);
#           stop reading output
            $g_shift_found = 1;
            printf "g-shift\n";
            mprint(@g_shift);
            last;
         }
      }
      close FL;

      die "g-shift-tensor not found in frame $frame !\n" 
         if ( $g_shift_found == 0 );

      printf "isotropic_g_shift\n";
      printf "%10.2f\n",($g_shift[0]+$g_shift[4]+$g_shift[8])/3;
      $g_shift_iso_before += ($g_shift[0]+$g_shift[4]+$g_shift[8])/3;

      printf "N-O_vector\n";
      printf "%10.2f%10.2f%10.2f\n", $vec_x[0], $vec_x[1], $vec_x[2];

#     define rotation matrix according to N-O and C-C axes
      @pa = ((@vec_x), (@vec_y), (@vec_z));

      printf "rotation_matrix_U\n";
      mprint(@pa);
      printf "U_*_UT\n";
      mprint(mmult(@pa,mT(@pa)));

      printf "N-O_vector_after_rotation\n";
      printf "%10.2f%10.2f%10.2f\n", 
       $pa[0]*$vec_x[0]+$pa[1]*$vec_x[1]+$pa[2]*$vec_x[2],
       $pa[3]*$vec_x[0]+$pa[4]*$vec_x[1]+$pa[5]*$vec_x[2],
       $pa[6]*$vec_x[0]+$pa[7]*$vec_x[1]+$pa[8]*$vec_x[2];

#     rotate g-shift tensor according to N-O and C-C axes
      (@new_g_shift) = mmult(mmult(@pa,@g_shift),mT(@pa));

      printf "isotropic_g_shift_after_rotation\n";
      printf "%10.2f\n",($new_g_shift[0]+$new_g_shift[4]+$new_g_shift[8])/3;
      $g_shift_iso_after += ($new_g_shift[0]+$new_g_shift[4]+$new_g_shift[8])/3;

      printf "g-shift_after_rotation\n";
      mprint(@new_g_shift);

      for $i ( 0 .. 8 )
      {
         $aver_g_shift[$i] += $new_g_shift[$i];
      }

   }

   for $i ( 0 .. 8 )
   {
      $aver_g_shift[$i] /= $nFrames;
   }
   printf "\n";
   printf "===============================\n";
   printf "Average_g_shift\n";
   mprint(@aver_g_shift);
   printf "===============================\n";
   printf "g_shift_iso_before_rotation %10.2f\n", $g_shift_iso_before/$nFrames;
   #printf "g_shift_iso_after_rotation  %10.2f\n", $g_shift_iso_after/$nFrames;
   printf "g_shift_iso_after_rotation  %10.2f\n", ($aver_g_shift[0]+$aver_g_shift[4]+$aver_g_shift[8])/3.0;
   printf "===============================\n";


#  matrix multiplication
   sub mmult
   {
      die "Incorrect number of elements in mmult!\n"
         if @_!=18;
      my ($i,$j,$k);
      my (@a,@b,@c);
      for $i ( 0 .. 8 )
      {
         $a[$i] = $_[$i];
         $b[$i] = $_[$i+9];
      }
      for $i ( 0 .. 2 )
      {
         for $j ( 0 .. 2 )
         {
            $c[$i*3+$j] = 0.0;
            for $k ( 0 .. 2 )
            {
               $c[$i*3+$j] += $a[$i*3+$k] * $b[$k*3+$j];
            }
         }
      }
      return (@c);
   }

#  matrix transpose
   sub mT
   {
      my ($i,$j);
      my @a;
      for $i ( 0 .. 2 )
      {
         for $j ( 0 .. 2 )
         {
            $a[$j*3+$i] = $_[$i*3+$j];
         }
      }
      return (@a);
   }

#  printf matrix
   sub mprint
   {
      for $i ( 0 .. 2 )
      {
         for $j ( 0 .. 2 )
         {
            printf "%10.2f", $_[$i*3+$j];
         }
         printf "\n";
      }
   }

#  outer product
   sub out_p
   {
      my ($i);
      my (@a,@b,@c);
      for $i ( 0 .. 2 )
      {
         $a[$i] = $_[$i];
         $b[$i] = $_[$i+3];
      }
      $c[0] = $a[1]*$b[2] - $a[2]*$b[1];
      $c[1] = $a[2]*$b[0] - $a[0]*$b[2];
      $c[2] = $a[0]*$b[1] - $a[1]*$b[0];
      return (@c);
   }

