#!/usr/bin/perl

   use strict;
   use warnings;
   use Math::Trig;

   my ($file,$frame,$line,$oxygen,$hydrogen);

#  get filenames
   my @outfiles=`ls *.hfc.out`;
   my $nFiles=@outfiles;
   my $nFrames=@outfiles;
   for $file ( 1 .. @outfiles )
   {
      chomp($outfiles[$file-1]);
      $outfiles[$file-1] =~ s/W_//;
      $outfiles[$file-1] =~ s/\.qmmm\.hfc\.out//;
   }

   my $aver_tilt = 0.0;

   my $hfcc_before = 0.0;
   my $hfcc_after = 0.0;

   for $file ( 1 .. $nFiles )
   {
      $frame = $outfiles[$file-1];

#     read coordinates and compute N-O vector
      my (@a,@x,@y,@z);
      my $atom = 0;
      my $mol_file="W_".$frame."_mol.inp";
      open MOL,"$mol_file" or die "Cannot open file $mol_file !\n";
      while ( <MOL> )
      {
         if ( /^C / or /^H / or /^O / or /^N / or /^S / )
         {
            $atom++;
            $a[$atom] = (split)[0];
            $x[$atom] = (split)[1];
            $y[$atom] = (split)[2];
            $z[$atom] = (split)[3];
         }
      }
      close MOL;
      die "Incorrect number of atoms!\n" unless $atom==71;

      my $x0 = $x[60];
      my $y0 = $y[60];
      my $z0 = $z[60];

      my $num_HB = 0;
      my $aver_r_ON_OW = 0.0;
      my $aver_r_ON_HW = 0.0;

      my $pot_file="W_".$frame."_pot.inp";
      open POT,"$pot_file" or die "Cannot open file $pot_file !\n";
      $_ = <POT>; $_ = <POT>;
      for $line ( 1 .. 2 ) { $_ = <POT>; }
      for $oxygen ( 1 .. 2773 )
      {
         $_ = <POT>; @_ = (split);
         my $xo = $_[0];
         my $yo = $_[1];
         my $zo = $_[2];
         die unless $_[3]==-0.834;
         my $r_ON_OW = sqrt( ($x0-$xo)**2 + ($y0-$yo)**2 + ($z0-$zo)**2 );

         for $hydrogen ( 1 .. 2 )
         {
            $_ = <POT>; @_ = (split);
            my $xh = $_[0];
            my $yh = $_[1];
            my $zh = $_[2];
            die unless $_[3]==0.417;
            my $r_ON_HW = sqrt( ($x0-$xh)**2 + ($y0-$yh)**2 + ($z0-$zh)**2 );

            if ( $r_ON_OW<=3.3 and $r_ON_HW<=2.7 )
            {
               $num_HB++;
               $aver_r_ON_OW += sqrt( ($x0-$xo)**2 + ($y0-$yo)**2 + ($z0-$zo)**2 );
               $aver_r_ON_HW += sqrt( ($x0-$xh)**2 + ($y0-$yh)**2 + ($z0-$zh)**2 );
            }
         }

      }
      close POT;

      if ( $num_HB > 0 )
      {
         $aver_r_ON_OW /= $num_HB;
         $aver_r_ON_HW /= $num_HB;
      }
      printf ( "%d  %d  %.6f  %.6f\n", $frame, $num_HB, $aver_r_ON_OW, $aver_r_ON_HW );

   }


