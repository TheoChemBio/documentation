#!/usr/bin/perl

   use strict;
   use warnings;
   use Math::Trig;

   my ($file, $frame, $hfc_found, $i, $j, $k, $aii, $aji);
   my (@hfc, @pa, @new_hfc);

#  get filenames
   my @outfiles=`ls *.hfc.out`;
   my $nFiles=@outfiles;
   my $nFrames=@outfiles;
   for $file ( 1 .. @outfiles )
   {
      chomp($outfiles[$file-1]);
      $outfiles[$file-1] =~ s/W_//;
      $outfiles[$file-1] =~ s/\.qmmm\.hfc\.out//;
   }

   my @aver_hfc;
   for $i ( 0 .. 8 )
   {
      $aver_hfc[$i] = 0.0;
   }

   my $hfcc_before = 0.0;
   my $hfcc_after = 0.0;

   for $file ( 1 .. $nFiles )
   {
      $frame = $outfiles[$file-1];
      printf "\nFrame $frame\n";

#     read coordinates and compute N-O vector
      my (@a,@x,@y,@z);
      my $atom = 0;
      my $mol_file="W_".$frame."_mol.inp";
      open MOL,"$mol_file" or die "Cannot open file $mol_file !\n";
      while ( <MOL> )
      {
         if ( /^C / or /^H / or /^O / or /^N / or /^S / )
         {
            $atom++;
            $a[$atom] = (split)[0];
            $x[$atom] = (split)[1];
            $y[$atom] = (split)[2];
            $z[$atom] = (split)[3];
         }
      }
      close MOL;
      die "Incorrect number of atoms!\n" unless $atom==71;

      my $r_no=sqrt( ($x[60]-$x[66])**2 
                   + ($y[60]-$y[66])**2 
                   + ($z[60]-$z[66])**2 );
      my @vec_x=( ($x[60]-$x[66])/$r_no,
                  ($y[60]-$y[66])/$r_no, 
                  ($z[60]-$z[66])/$r_no );

      my $r_cc=sqrt( ($x[1]-$x[2])**2 
                   + ($y[1]-$y[2])**2 
                   + ($z[1]-$z[2])**2 );
      my @vec_y=( ($x[1]-$x[2])/$r_cc, 
                  ($y[1]-$y[2])/$r_cc,
                  ($z[1]-$z[2])/$r_cc );

#     NOTE: x and y are not necessarily perpendicular
#           so z must be normalized!
      my @vec_z=out_p(@vec_x,@vec_y);
      my $r_vec_z = sqrt($vec_z[0]**2+$vec_z[1]**2+$vec_z[2]**2);
      $vec_z[0] /= $r_vec_z;
      $vec_z[1] /= $r_vec_z;
      $vec_z[2] /= $r_vec_z;

      @vec_y=out_p(@vec_z,@vec_x);

#     read HFC tensor (principal axes and values) from output
      $hfc_found = 0;
      open FL,"W_$frame.qmmm.hfc.out"
         or die "Cannot open file W_$frame.qmmm.hfc.out !\n";
      while ( <FL> )
      {
         if ( /\| 66N  \|   14  \|   <SD>   \|/ )
         {

# | Atom | Isot. | Contrib. |  A_xx   |  A_yy    |  A_zz    |  A_xy    |   A_xz   |  A_yz    |
# | 66N  |   14  |   <SD>   |   13.12 |    -9.08 |    -4.04 |     1.80 |   -11.24 |    -1.15 |
# |--------------| <<SD;H>> |    0.58 |    -0.65 |     0.07 |     0.30 |    -0.48 |     0.33 |
# |              |   Total  |   13.70 |    -9.73 |    -3.97 |     2.10 |   -11.72 |    -0.82 |

#           read next line and get info of <<SD;H>>
            $_ = <FL>;
            $_ = (split "<<SD;H>>",$_)[1];
            s/\|//g;
            @_ = (split);

            $hfc[0] = $_[0];
            $hfc[4] = $_[1];
            $hfc[8] = $_[2];

            $hfc[1] = $_[3];
            $hfc[3] = $_[3];

            $hfc[2] = $_[4];
            $hfc[6] = $_[4];

            $hfc[5] = $_[5];
            $hfc[7] = $_[5];

#           stop reading output
            $hfc_found = 1;
            printf "HFC_tensor\n";
            mprint(@hfc);
            last;
         }
      }
      close FL;

      die "HFC tensor not found in frame $frame !\n" 
         if ( $hfc_found == 0 );

      printf "isotropic_hfcc\n";
      printf "%8.2f\n",($hfc[0]+$hfc[4]+$hfc[8])/3;
      $hfcc_before += ($hfc[0]+$hfc[4]+$hfc[8])/3;

      printf "N-O_vector\n";
      printf "%8.2f%8.2f%8.2f\n", $vec_x[0], $vec_x[1], $vec_x[2];

#     define rotation matrix according to N-O and C-C axes
      @pa = ((@vec_x), (@vec_y), (@vec_z));

      printf "rotation_matrix_U\n";
      mprint(@pa);
      printf "U_*_UT\n";
      mprint(mmult(@pa,mT(@pa)));

      printf "N-O_vector_after_rotation\n";
      printf "%8.2f%8.2f%8.2f\n", 
         $pa[0]*$vec_x[0]+$pa[1]*$vec_x[1]+$pa[2]*$vec_x[2],
         $pa[3]*$vec_x[0]+$pa[4]*$vec_x[1]+$pa[5]*$vec_x[2],
         $pa[6]*$vec_x[0]+$pa[7]*$vec_x[1]+$pa[8]*$vec_x[2];

#     rotate HFC tensor according to N-O and C-C axes
      (@new_hfc) = mmult(mmult(@pa,@hfc),mT(@pa));

      printf "isotropic_hfcc_after_rotation\n";
      printf "%8.2f\n",($new_hfc[0]+$new_hfc[4]+$new_hfc[8])/3;
      $hfcc_after += ($new_hfc[0]+$new_hfc[4]+$new_hfc[8])/3;

      printf "HFC_tensor_after_rotation\n";
      mprint(@new_hfc);

      for $i ( 0 .. 8 )
      {
         $aver_hfc[$i] += $new_hfc[$i];
      }

   }

   for $i ( 0 .. 8 )
   {
      $aver_hfc[$i] /= $nFrames;
   }
   printf "\n";
   printf "=============================\n";
   printf "Average_HFC\n";
   mprint(@aver_hfc);
   printf "=============================\n";
   printf "HFCC_before_rotation %8.2f\n", $hfcc_before/$nFrames;
   #printf "HFCC_after_rotation  %8.2f\n", $hfcc_after/$nFrames;
   printf "HFCC_after_rotation  %8.2f\n", ($aver_hfc[0]+$aver_hfc[4]+$aver_hfc[8])/3.0;
   printf "=============================\n";


#  matrix multiplication
   sub mmult
   {
      die "Incorrect number of elements in mmult!\n"
         if @_!=18;
      my ($i,$j,$k);
      my (@a,@b,@c);
      for $i ( 0 .. 8 )
      {
         $a[$i] = $_[$i];
         $b[$i] = $_[$i+9];
      }
      for $i ( 0 .. 2 )
      {
         for $j ( 0 .. 2 )
         {
            $c[$i*3+$j] = 0.0;
            for $k ( 0 .. 2 )
            {
               $c[$i*3+$j] += $a[$i*3+$k] * $b[$k*3+$j];
            }
         }
      }
      return (@c);
   }

#  matrix transpose
   sub mT
   {
      my ($i,$j);
      my @a;
      for $i ( 0 .. 2 )
      {
         for $j ( 0 .. 2 )
         {
            $a[$j*3+$i] = $_[$i*3+$j];
         }
      }
      return (@a);
   }

#  printf matrix
   sub mprint
   {
      for $i ( 0 .. 2 )
      {
         for $j ( 0 .. 2 )
         {
            printf "%8.2f", $_[$i*3+$j];
         }
         printf "\n";
      }
   }

#  outer product
   sub out_p
   {
      my ($i);
      my (@a,@b,@c);
      for $i ( 0 .. 2 )
      {
         $a[$i] = $_[$i];
         $b[$i] = $_[$i+3];
      }
      $c[0] = $a[1]*$b[2] - $a[2]*$b[1];
      $c[1] = $a[2]*$b[0] - $a[0]*$b[2];
      $c[2] = $a[0]*$b[1] - $a[1]*$b[0];
      return (@c);
   }

