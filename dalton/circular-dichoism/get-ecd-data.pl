#!/usr/bin/perl

use strict;
use warnings;
use Math::Trig;

#----------Constants--------------

# pi = 3.141592653589793238462643 
# epsilon_0 = 8.854187817e-12 F m^-1 
# ea0 = 8.47835326e-30 C m 

# ea0 = 2.54174636369676 Debye 
#     = 2.54174636369676e-18 esu cm 

# Bohr magneton = 9.27400968e-24 J T^-1 
#               = 9.27400968e-21 erg G^-1 

# ea0 x Bohr magneton = 235.721803810286e-40 erg esu cm G^-1 

#EaBm2CGS = 235.721803810286;

#ea2CGS = 2.54174636369676e-18;
#BM2CGS = 9.27400968e-21;

#DDFactor  = 9.185929163775e-39; # cgs unit 
#DBMFactor = 2.296482290944e-39; # cgs unit 

# DBM2theta = 24.0 pi NA / h_bar c (in cgs unit) 
#           = 48.0*pi*pi * 6.02214129e+23 / (6.62606957e-27 * 2.99792458e+10) 

#DBM2theta = 1.43620101403094e+42;

#au2eps   = $ea2CGS * $ea2CGS / $DDFactor;  # a.u. to M^-1 cm^-1 (abs) 
#au2theta = $ea2CGS * $BM2CGS * $DBM2theta; # a.u. to deg cm^2 dmol^-1 (ecd) 

# J. A. Schellman, Chem Rev 1975, 75, 323-331. */
#theta2eps = pi / (4.5 * log(10.0) * 1000.0);

my $abs2eps = 703.301153555896; # from dipole_strength ((ea0)^2) to M^-1 cm^-1
my $ecd2eps = 10.2644729610965; # from rotatory_strength (ea0xBM) to M^-1 cm^-1

# Physical constants 
# http://physics.nist.gov/cgi-bin/cuu/Value?hrev 
# http://physics.nist.gov/cuu/Constants/Preprints/lsa2010.pdf 

my $Eh2eV = 27.21138505;
my $eV2Eh = 1.0/27.21138505;
my $Ehxnm = 1.0e+02/2.194746313708;
my $eVxnm = 1.0e+04/8.06554429;

#-----end of constans------------

my ($wavMin, $wavMax) = (150, 500);
my ($wl5, @averECD, @averABS, @ECD, @ABS);
my ($j, $k);

for ($wl5=$wavMin*5; $wl5<$wavMax*5; $wl5++)
{
    $averECD[$wl5] = 0.0;
    $averABS[$wl5] = 0.0;

    for $j ( 0 .. 5 )
    {
        for $k ( 0 .. 5 )
        {
            $ECD[$wl5][$j][$k] = 0.0;
            $ABS[$wl5][$j][$k] = 0.0;
        }
    }
}

$_ = `ls ecd*out`;
my @file = split;
my $ifile;

my $nStates = 20;
my ($n, $n_homo, @moComp);

for $n ( 1 .. $nStates )
{
    for $j ( 0 .. 5 )
    {
        for $k ( 0 .. 5 )
        {
            $moComp[$n][$j][$k] = 0.0;
        }
    }
}

for $ifile ( 0 .. @file-1 )
{
    #printf "Parsing file $file[$ifile] ...\n";

    # zero MO compositions

    for $n ( 1 .. $nStates )
    {
        for $j ( 0 .. 5 )
        {
            for $k ( 0 .. 5 )
            {
                $moComp[$n][$j][$k] = 0.0;
            }
        }
    }

    # get excitations

    open FL,"$file[$ifile]" or die;

    $n = 0;
    while(<FL>)
    {
        if (/Electrons in DFTMOMO:/)
        {
            s/Electrons in DFTMOMO://;
            $n_homo = sprintf "%.0f", (split)[0] / 2;
            $n_homo += 0;
        }

        if (/@ Excitation energy :/)
        {
            $n ++;
        }

        if (/Index\(r,s\)/)
        {
            $_ = <FL>;
            while(<FL>)
            {
                last if (/^\s*$/);
                @_ = split;
                $_[1] =~ s/\(1\)//;
                $_[2] =~ s/\(1\)//;
                $_[3] = $_[3]**2 * 2 * 100;

                my $moI = $n_homo-$_[1];
                my $moF = $_[2]-1-$n_homo;
                if ($moI<=5 and $moF<=5)
                {
                    $moComp[$n][$moI][$moF] = $_[3];
                }
            }
        }
    }
    close FL;

    # get info for circular dichroism

    my (@eneExc, @eDip, @vDip, @mDip);
    my (@dipStr, @rotLen, @rotVel);

    for $n ( 1 .. $nStates )
    {
        my $string = sprintf "STATE NO:   %2d", $n;

        # electric dipole
        my $icar;
        my @diplen = qw/XDIPLEN YDIPLEN ZDIPLEN/;
        for $icar ( 0 .. 2 )
        {
            $_ = `grep -A$nStates 'Transition operator type:    $diplen[$icar]' $file[$ifile] | grep '$string'`;
            if (/STATE NO:\s+(\S+)\s+\*TRANSITION MOMENT:\s+(\S+)\s+\*ENERGY\(eV\):\s+(\S+)/)
            {
                $eneExc[$n] = $3;
                $eDip[$n][$icar] = $2;
            }
            else
            {
                die "Error in line: $_";
            }
        }

        # velocity dipole
        my @dipvel = qw/XDIPVEL YDIPVEL ZDIPVEL/;
        for $icar ( 0 .. 2 )
        {
            $_ = `grep -A$nStates 'Transition operator type:    $dipvel[$icar]' $file[$ifile] | grep '$string'`;
            if (/STATE NO:\s+(\S+)\s+\*TRANSITION MOMENT:\s+(\S+)\s+\*ENERGY\(eV\):\s+(\S+)/)
            {
                $eneExc[$n] = $3;
                $vDip[$n][$icar] = $2;
            }
            else
            {
                die "Error in line: $_";
            }
        }

        # magnetic dipole
        my @angmom = qw/XANGMOM YANGMOM ZANGMOM/;
        for $icar ( 0 .. 2 )
        {
            $_ = `grep -A$nStates 'Transition operator type:    $angmom[$icar]' $file[$ifile] | grep '$string'`;
            if (/STATE NO:\s+(\S+)\s+\*TRANSITION MOMENT:\s+(\S+)\s+\*ENERGY\(eV\):\s+(\S+)/)
            {
                $eneExc[$n] = $3;
                $mDip[$n][$icar] = $2;
            }
            else
            {
                die "Error in line: $_";
            }
        }
    }

    for $n ( 1 .. $nStates )
    {
        $eneExc[$n] *= $eV2Eh;

        $dipStr[$n] = $eDip[$n][0]**2 +
                      $eDip[$n][1]**2 + 
                      $eDip[$n][2]**2;

        $rotLen[$n] = ( $eDip[$n][0] * $mDip[$n][0] +
                        $eDip[$n][1] * $mDip[$n][1] +
                        $eDip[$n][2] * $mDip[$n][2] ) * (-1.0);

        $rotVel[$n] = ( $vDip[$n][0] * $mDip[$n][0] +
                        $vDip[$n][1] * $mDip[$n][1] +
                        $vDip[$n][2] * $mDip[$n][2] ) / $eneExc[$n];

    ####
    ########
    ############
        $rotLen[$n] *= -1.0;
        $rotVel[$n] *= -1.0;
    ############
    ########
    ####
    }

    my $name = $file[$ifile];
    $name =~ s/\.out$//;
    open TXT,">$name.txt" or die;
    for ($wl5=$wavMin*5; $wl5<$wavMax*5; $wl5++)
    {
        my $wavLen = $wl5 / 5.0;

        my $absDipStr = 0.0;
        my $ecdRotLen = 0.0;
        my $ecdRotVel = 0.0;

        for $n ( 1 .. $nStates )
        {
            # del in eV, GNorm in eV^-1, preFact dimensionless 

            my $hbw = 0.2; #eV
            my $GNorm = 1.0 / ($hbw * sqrt(pi)); #eV^-1
            
            my $del = $eVxnm / $wavLen - $Eh2eV * $eneExc[$n]; #eV
            my $preFac = $GNorm * exp(-($del / $hbw)**2) * ($eVxnm / $wavLen);

            # del in nm, GNorm in nm^-1, preFact dimensionless 

            $absDipStr += $dipStr[$n] * $preFac * $abs2eps; # M^-1 cm^-1 
            $ecdRotLen += $rotLen[$n] * $preFac * $ecd2eps; # M^-1 cm^-1 
            $ecdRotVel += $rotVel[$n] * $preFac * $ecd2eps; # M^-1 cm^-1 

            for $j (0 .. 5)
            {
                for $k (0 .. 5)
                {
                    $ECD[$wl5][$j][$k] += 
                        $rotVel[$n] * $preFac * $ecd2eps * $moComp[$n][$j][$k]/100.0;
                    $ABS[$wl5][$j][$k] += 
                        $dipStr[$n] * $preFac * $abs2eps * $moComp[$n][$j][$k]/100.0;
                }
            }
        }

        printf TXT "%10.2f%20.10e%20.10e%20.10e\n", 
                $wavLen, $ecdRotVel, $ecdRotLen, $absDipStr;
        $averECD[$wl5] += $ecdRotVel;
        $averABS[$wl5] += $absDipStr;
    }
    close TXT;

    open DAT,">$name.dat" or die;
    printf DAT "%-5s%10s%10s%10s%10s%10s%10s%10s%10s\n", 
            "State", "E_exc", "Osc.Str", "R.S.Vel", "R.S.Len",
            "H0-L0", "H1-L0", "H0-L1", "H1-L1";
    for $n ( 1 .. $nStates )
    {
        printf DAT "%-5d%10.3f%10.3f%10.1f%10.1f\n", 
                $n, 
                $eneExc[$n] * $Eh2eV,
                $dipStr[$n] * $eneExc[$n] * 2.0/3.0,
                $rotVel[$n] * 235.721803810286,
                $rotLen[$n] * 235.721803810286;
    }
    close DAT;
}

#
# average ABS and ECD
#

for ($wl5=$wavMin*5; $wl5<$wavMax*5; $wl5++)
{
    $averECD[$wl5] /= (@file+0);
    $averABS[$wl5] /= (@file+0);

    for $j (0 .. 5)
    {
        for $k (0 .. 5)
        {
            $ECD[$wl5][$j][$k] /= (@file+0);
            $ABS[$wl5][$j][$k] /= (@file+0);
        }
    }
}

#
# get contributions from each transition
#

my ($pSum, $mSum) = (0.0, 0.0);
my (@pCtrb, @mCtrb);
for $j (0 .. 5)
{
    for $k (0 .. 5)
    {
        $pCtrb[$j][$k] = 0.0;
        $mCtrb[$j][$k] = 0.0;
    }
}

for ($wl5=$wavMin*5; $wl5<$wavMax*5; $wl5++)
{
    $pSum += $averECD[$wl5] if $averECD[$wl5] >= 0;
    $mSum += $averECD[$wl5] if $averECD[$wl5] <  0;
    for $j (0 .. 5)
    {
        for $k (0 .. 5)
        {
            $pCtrb[$j][$k] += $ECD[$wl5][$j][$k] if $ECD[$wl5][$j][$k] >= 0;
            $mCtrb[$j][$k] += $ECD[$wl5][$j][$k] if $ECD[$wl5][$j][$k] <  0;
        }
    }
}

my @important;
for $j (0 .. 5)
{
    for $k (0 .. 5)
    {
        $important[$j][$k] = 0;
        next if ($pCtrb[$j][$k]/$pSum<0.05 and $mCtrb[$j][$k]/$mSum<0.05);

        $important[$j][$k] = 1;
        printf "Contrib. H$j->L$k = (+)%-6.0f (-)%-6.0f\n", 
                 $pCtrb[$j][$k]/$pSum*100, $mCtrb[$j][$k]/$mSum*100;
    }
}

#
# print spectra and individual contributions
#

open AVA,">aver-abs.txt" or die;
open AVE,">aver-ecd.txt" or die;
open AVG,">aver-g.txt" or die;
for ($wl5=$wavMin*5; $wl5<$wavMax*5; $wl5++)
{
   printf AVA "%10.2f%20.10e\n", 
             $wl5/5.0, $averABS[$wl5];

   printf AVE "%10.2f%20.10e", $wl5/5.0, $averECD[$wl5];
    for $j (0 .. 5)
    {
        for $k (0 .. 5)
        {
            next if $important[$j][$k] == 0;
            printf AVE "%20.10e", $ECD[$wl5][$j][$k];
        }
    }
    printf AVE "\n";

    my $gg = 0.0;
    $gg = $averECD[$wl5]/$averABS[$wl5] if abs($averABS[$wl5]) > 0.0;
   printf AVG "%10.2f%20.10e\n", $wl5/5.0, $gg;
}
close AVA;
close AVE;
close AVG;

#
# find max
#

my $lambda_max;
my $delta_eps_max = -1e10;
for ($wl5=$wavMax*5-1; $wl5>=$wavMin*5; $wl5--)
{
    if ($averECD[$wl5] > $delta_eps_max)
    {
        $delta_eps_max = $averECD[$wl5];
        $lambda_max = $wl5 / 5.0;
    }
}
printf "excit_ene_max = %.3f eV\n", $eVxnm / $lambda_max;
printf "delta_eps_max = %.1f M^-1 cm^-1\n", $delta_eps_max;

my $eps_max;
for ($wl5=$wavMax*5-1; $wl5>=$wavMin*5+1; $wl5--)
{
    if ($averABS[$wl5] > $averABS[$wl5-1])
    {
        $eps_max = $averABS[$wl5];
        $lambda_max = $wl5 / 5.0;
        last;
    }
}
printf "=====\n";
printf "ene_max = %.3f eV\n", $eVxnm / $lambda_max;
printf "eps_max = %.1f M^-1 cm^-1\n", $eps_max;

