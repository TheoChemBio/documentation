#!/usr/bin/perl

use strict;
use warnings;

die unless @ARGV==1;
my $file = $ARGV[0];

my $Eh2eV = 27.21138505;
my $eV2Eh = 1.0 / 27.21138505;

my $ea2CGS = 2.54174636369676e-18;
my $BM2CGS = 9.27400968e-21;

#=====================================================================
#
# Coupled cluster response calculation of natural chiroptical spectra
# Thomas Bondo Pedersen, Henrik Koch, and Kenneth Ruud
# J. Chem. Phys. 110, 2883 (1999); doi: 10.1063/1.477931
#
# --variables--
# n: the n-th excited state
# omega: excitation energy
# oprt_p: momentum operator (velocity dipole)
# oprt_L: angular momentum operator (magnetic dipole)
#
# j, k, m, l: indices
#
# --ECD tensor--
# mten: electric dipole-magnetic dipole contribution
# qten: electric dipole-electric quadrupole contribution
#
#=====================================================================

my $nStates = 20;
my (@omega, @oprt_p, @oprt_L, @oprt_prrp);
my (@rotVel);

my $n;
for $n ( 1 .. $nStates )
{
    my $string = sprintf "STATE NO:   %2d", $n;
    my $icar;

    my @dipvel = qw/XDIPVEL YDIPVEL ZDIPVEL/;
    for $icar ( 0 .. 2 )
    {
        $_ = `grep -A$nStates 'Transition operator type:    $dipvel[$icar]' $file | grep '$string'`;
        if (/STATE NO:\s+(\S+)\s+\*TRANSITION MOMENT:\s+(\S+)\s+\*ENERGY\(eV\):\s+(\S+)/)
        {
            $omega[$n] = $3 * $eV2Eh;
            $oprt_p[$n][$icar] = $2;
        }
        else
        {
            die "Error in line: $_";
        }
    }

    my @angmom = qw/XANGMOM YANGMOM ZANGMOM/;
    for $icar ( 0 .. 2 )
    {
        $_ = `grep -A$nStates 'Transition operator type:    $angmom[$icar]' $file | grep '$string'`;
        if (/STATE NO:\s+(\S+)\s+\*TRANSITION MOMENT:\s+(\S+)\s+\*ENERGY\(eV\):\s+(\S+)/)
        {
            $omega[$n] = $3 * $eV2Eh;
#
##
###
            $oprt_L[$n][$icar] = $2 * (-1.0);
###
##
#
        }
        else
        {
            die "Error in line: $_";
        }
    }

    my @rotstr = ( [ qw/XXROTSTR XYROTSTR XZROTSTR/ ],
                   [ qw/XYROTSTR YYROTSTR YZROTSTR/ ],
                   [ qw/XZROTSTR YZROTSTR ZZROTSTR/ ] );
    my ($j, $k);
    for $j ( 0 .. 2 )
    {
        for $k ( 0 .. 2)
        {
            $_ = `grep -A$nStates 'Transition operator type:    $rotstr[$j][$k]' $file | grep '$string'`;
            if (/STATE NO:\s+(\S+)\s+\*TRANSITION MOMENT:\s+(\S+)\s+\*ENERGY\(eV\):\s+(\S+)/)
            {
                $omega[$n] = $3 * $eV2Eh;
                $oprt_prrp[$n][$j][$k] = $2;
            }
            else
            {
                die "Error in line: $_";
            }
        }
    }
}

print "Velocity Dipole\n";
printf "%12s %12s %10s %10s %10s\n", "State", "E_au", "X", "Y", "Z";
for $n ( 1 .. $nStates )
{
    printf "%12d %12.6f %10.4f %10.4f %10.4f\n", 
            $n, $omega[$n], 
            $oprt_p[$n][0] / $omega[$n], 
            $oprt_p[$n][1] / $omega[$n], 
            $oprt_p[$n][2] / $omega[$n];
}

print "Magnetic Dipole\n";
printf "%12s %12s %10s %10s %10s\n", "State", "E_eV", "X", "Y", "Z";
for $n ( 1 .. $nStates )
{
    printf "%12d %12.6f %10.4f %10.4f %10.4f\n", 
            $n, $omega[$n] * $Eh2eV, 
            $oprt_L[$n][0] * 0.5,
            $oprt_L[$n][1] * 0.5,
            $oprt_L[$n][2] * 0.5;
}

print "Quadrupole\n";
printf "%12s %12s %10s %10s %10s\n", "State", "E_au", "XX", "YY", "ZZ";
for $n ( 1 .. $nStates )
{
    printf "%12d %12.6f %10.4f %10.4f %10.4f\n", 
            $n, $omega[$n],
            $oprt_prrp[$n][0][0] / $omega[$n], 
            $oprt_prrp[$n][1][1] / $omega[$n], 
            $oprt_prrp[$n][2][2] / $omega[$n];
}

print "Quadrupole\n";
printf "%12s %12s %10s %10s %10s\n", "State", "E_au", "XY", "XZ", "YZ";
for $n ( 1 .. $nStates )
{
    printf "%12d %12.6f %10.4f %10.4f %10.4f\n", 
            $n, $omega[$n],
            $oprt_prrp[$n][0][1] / $omega[$n], 
            $oprt_prrp[$n][0][2] / $omega[$n], 
            $oprt_prrp[$n][1][2] / $omega[$n];
}

print "Rot.Str.(Vel)\n";
printf "%12s %12s %10s\n", "State", "E_eV", "RSVel";
for $n ( 1 .. $nStates )
{
    $rotVel[$n] = ( $oprt_p[$n][0] * $oprt_L[$n][0] +
                    $oprt_p[$n][1] * $oprt_L[$n][1] +
                    $oprt_p[$n][2] * $oprt_L[$n][2] ) / $omega[$n];
    printf "%12d %12.6f %10.4f\n", 
            $n, 
            $omega[$n] * $Eh2eV, 
            $rotVel[$n] * $ea2CGS * $BM2CGS * 1.0e+40;
}

#
# calculate mten and qten
#

my ($j, $k, $m, $l);
my (@mten, @qten);

# mten
for $n ( 1 .. $nStates )
{
    my $inn_prd = $oprt_p[$n][0] * $oprt_L[$n][0] + 
                  $oprt_p[$n][1] * $oprt_L[$n][1] +
                  $oprt_p[$n][2] * $oprt_L[$n][2];
    for $j (0 .. 2)
    {
        for $k (0 .. 2)
        {
            $mten[$n][$j][$k] = 
                (Kronecker($j, $k) * $inn_prd - 
                 $oprt_p[$n][$j] * $oprt_L[$n][$k]) *
                3.0 / (4.0 * $omega[$n]);

            # ... convert a.u. to Bohr magneton by dividing 1/2 ... #
            $mten[$n][$j][$k] /= 0.5;

            # ... convert e.a.B.M to CGS unit ... #
            $mten[$n][$j][$k] *= $ea2CGS * $BM2CGS * 1.0e+40;
        }
    }
}

# qten
for $n ( 1 .. $nStates )
{
    for $j (0 .. 2)
    {
        for $k (0 .. 2)
        {
            $qten[$n][$j][$k] = 0.0;
            for $l (0 .. 2)
            {
                for $m (0 .. 2)
                {
                    $qten[$n][$j][$k] += 
                        Levi_Civita($j, $l, $m) * 
                        $oprt_p[$n][$l] * $oprt_prrp[$n][$m][$k];
                }
            }
            $qten[$n][$j][$k] *= (-3.0 / (4.0 * $omega[$n]));

            # ... convert a.u. to Bohr magneton by dividing 1/2 ... #
            $qten[$n][$j][$k] /= 0.5;

            # ... convert e.a.B.M to CGS unit ... #
            $qten[$n][$j][$k] *= $ea2CGS * $BM2CGS * 1.0e+40;
        }
    }
}


####
my (@sym_mten, @sym_qten, @tot_ten);
for $n ( 1 .. $nStates )
{
    for $j (0 .. 2)
    {
        for $k (0 .. 2)
        {
            $sym_mten[$n][$j][$k] = 0.5 * ($mten[$n][$j][$k] + $mten[$n][$k][$j]);
            $sym_qten[$n][$j][$k] = 0.5 * ($qten[$n][$j][$k] + $qten[$n][$k][$j]);
        }
    }
}
for $n ( 1 .. $nStates )
{
    for $j (0 .. 2)
    {
        for $k (0 .. 2)
        {
            $mten[$n][$j][$k] = $sym_mten[$n][$j][$k];
            $qten[$n][$j][$k] = $sym_qten[$n][$j][$k];
            $tot_ten[$n][$j][$k] = $mten[$n][$j][$k] + $qten[$n][$j][$k];;
        }
    }
}
####

print "ECD m tensor\n";
printf "%12s %10s %10s %10s %10s %10s %10s\n", 
         "State", "XX", "XY", "XZ", "YY", "YZ", "YZ";
for $n ( 1 .. $nStates )
{
    printf "%12d %10.4f %10.4f %10.4f %10.4f %10.4f %10.4f\n", 
            $n, $mten[$n][0][0], $mten[$n][0][1], $mten[$n][0][2],
            $mten[$n][1][1], $mten[$n][1][2], $mten[$n][2][2];
}

print "ECD q tensor\n";
printf "%12s %10s %10s %10s %10s %10s %10s\n", 
         "State", "XX", "XY", "XZ", "YY", "YZ", "YZ";
for $n ( 1 .. $nStates )
{
    printf "%12d %10.4f %10.4f %10.4f %10.4f %10.4f %10.4f\n", 
            $n, $qten[$n][0][0], $qten[$n][0][1], $qten[$n][0][2],
            $qten[$n][1][1], $qten[$n][1][2], $qten[$n][2][2];
}

print "Total ECD tensor\n";
printf "%12s %10s %10s %10s %10s %10s %10s\n", 
         "State", "XX", "XY", "XZ", "YY", "YZ", "YZ";
for $n ( 1 .. $nStates )
{
    printf "%12d %10.4f %10.4f %10.4f %10.4f %10.4f %10.4f\n", 
            $n, $tot_ten[$n][0][0], $tot_ten[$n][0][1], $tot_ten[$n][0][2],
            $tot_ten[$n][1][1], $tot_ten[$n][1][2], $tot_ten[$n][2][2];
}


# subroutines

sub Kronecker
{
    die unless @_ == 2;
    my ($a, $b) = @_;

    my $c;
    if ($a == $b)
    {
        $c = 1;
    }
    elsif ($a != $b)
    {
        $c = 0;
    }
    else
    {
        die "Error in subroutine Kronecker, a=$a, b=$b !\n";
    }

    return ($c);
}

sub Levi_Civita
{
    die unless @_ == 3;
    my ($a, $b, $c) = @_;

    my $d;
    if (($a==0 and $b==1 and $c==2) or
        ($a==1 and $b==2 and $c==0) or
        ($a==2 and $b==0 and $c==1))
    {
        $d = 1;
    }
    elsif (($a==0 and $b==2 and $c==1) or
           ($a==1 and $b==0 and $c==2) or
           ($a==2 and $b==1 and $c==0))
    {
        $d = -1;
    }
    elsif ($a==$b or $b==$c or $c==$a)
    {
        $d = 0;
    }
    else
    {
        die "Error in subroutine Levi_Civita, a=$a, b=$b, c=$c !\n";
    }

    return ($d);
}

