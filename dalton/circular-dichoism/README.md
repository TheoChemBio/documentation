[https://pubs.acs.org/doi/10.1021/jp412030t](https://pubs.acs.org/doi/10.1021/jp412030t)

Usage:

```
perl get-ecd-data.pl
perl get-ecd-tensor.pl ecd_W_1_W_1.out
```
