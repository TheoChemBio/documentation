#!/bin/bash
#SBATCH -N 1
#SBATCH -t 00:30:00
#SBATCH -A snic2018-7-13 
#SBATCH -J Q-CHEM

# JOB name
JOB=h2o

# Set number of threads for OpenMP (-nt) or MPI processes (-np) for parallel run
# NOTE: hybrid OpenMP+MPI not supported. More info: http://www.q-chem.com/qchem-website/manual/qchem51_manual/sec-parallel.html
nthreads=32
#nproc=32

export QC=/home/x_iubru/qchem_exe

# set QCAUX
export QCAUX=/home/x_iubru/qcaux

# Set scratch directory
export QCSCRATCH=$SNIC_TMP

# MPI
export QCRSH=ssh

# run qchem script
source $QC/bin/qchem.setup.sh

# load compiler libraries
source /software/sse/manual/intel/parallelstudioxe/2018.u1/compilers_and_libraries_2018.1.163/linux/bin/compilervars.sh intel64

qchem -nt $nthreads $JOB.in $JOB.out

