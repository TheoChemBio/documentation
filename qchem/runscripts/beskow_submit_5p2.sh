#!/bin/bash -l 
#SBATCH --nodes=1
#SBATCH -t 01:00:00
#SBATCH -c 18
#SBATCH -A 2019-3-147
#SBATCH -J AA_M_mp2
#SBATCH -o stdout.txt
#SBATCH -e stderr.txt

# Job name
JOB=AA_monomer_50

# MPI
export QCRSH=ssh
export QCMPI=craympi
echo $SLURM_NODELIST

# set path to qchem executables
export QC=/cfs/klemming/nobackup/i/iubr/qchem_5p2

# set QCAUX
export QCAUX=/cfs/klemming/nobackup/i/iubr/qcaux

# set scratch directory
export QCSCRATCH=$SNIC_TMP

# access shared libraries
export CRAY_ROOTFS=DSL

# run qchem script
source $QC/bin/qchem.setup.sh

# run qchem job 
qchem -nt 18 $JOB.in $JOB.out

